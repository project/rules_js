<?php

/**
 * @file
 * Example Adds a rules action to execute JavaScript via Drupal Messages
 */

/**
 * Implement hook_rules_action_info()

 * Declare any meta data about actions for Rules
 */
function rules_js_rules_action_info() {
  $actions = array(
    'rules_js_action_execute_js' => array(
      'label' => t('Execute custom JavaScript code'),
      'group' => t('Rules JS'),
      'parameter' => array(
        'code' => array(
          'restriction' => 'input',
          'type' => 'text',
          'label' => t('JavaScript code'),
          'description' => t('Enter JavaScript code <strong>INCLUDING &lt;script&gt; element</strong>. Don\'t output any other text or blanks so that the message will not be visible.'),
        ),
        'repeat' => array(
          'type' => 'boolean',
          'label' => t('Repeat message'),
          'description' => t("If disabled and the message has been already shown, then the message won't be repeated."),
          'default value' => TRUE,
          'optional' => TRUE,
          'restriction' => 'input',
        ),
      ),
    ),
  );

  return $actions;
}

/**
 * The action function for rules_example_action_hello_world
 */
function rules_js_action_execute_js($code, $repeat) {
  drupal_set_message($code, 'rules-js-message', $repeat);
}